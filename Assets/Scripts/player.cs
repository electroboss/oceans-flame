using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour
{
    Rigidbody playerRigidbody;
    private Vector3 currentEulerAngles;
    private Quaternion currentRotation;
    private const float linearSpeed = 10f;
    private const float rotationSpeed = 80f;
    private float movementHypotenuse;
    private Vector3 playerVelocity;

    // Start is called before the first frame update
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void FixedUpdate()
    {
        // Movement - Linear
        movementHypotenuse = Input.GetAxis("Vertical");

        playerVelocity.x = (float) Math.Sin(
            currentEulerAngles.y * Math.PI / 180 // convert degrees to radians
        ) * movementHypotenuse;
        
        playerVelocity.z = (float) Math.Cos(
            currentEulerAngles.y * Math.PI / 180 // convert degrees to radians
        ) * movementHypotenuse;


        // Movement - Rotational
        currentEulerAngles += new Vector3 (0,Input.GetAxis("Horizontal"),0)*Time.fixedDeltaTime*rotationSpeed;
        currentRotation.eulerAngles = currentEulerAngles;

        // Movement - Moving
        transform.rotation = currentRotation;
        playerRigidbody.MovePosition(transform.position + playerVelocity * Time.fixedDeltaTime * linearSpeed);
    }
}
