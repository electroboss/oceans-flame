using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AtriumPlayerScript : MonoBehaviour
{
    public GameObject text;
    public GameObject door;
    public GameObject fade;
    private Animation fadeAnim;
    private Animation doorAnim;
    private const int doorOpenThreshold = 5;
    private float dist;
    private bool switchScene;

    // Start is called before the first frame update
    void Start()
    {
        doorAnim = door.gameObject.GetComponent<Animation>();
        fadeAnim = fade.gameObject.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        // Detect if near door and show text
        dist = Vector3.Distance(door.transform.position, transform.position);
        text.SetActive(dist < doorOpenThreshold);
    }
    void FixedUpdate()
    {
        if (!fadeAnim.isPlaying && switchScene)
        {
            SceneManager.LoadScene(sceneName:"Scenes/Corridoor/Corridoor");
        }
        if (dist < doorOpenThreshold & Input.GetKeyDown(KeyCode.Q))
        {
            doorAnim.Play("door");
            fadeAnim.Play("fadeOut");
            switchScene = true;
        }
    }
}
